# Mogan Editor / [墨干编辑器](README_ZH.md)
Mogan Editor is a structured editor based on GNU TeXmacs.

Mogan is named after Mount Mogan[^1], a famous attraction in my hometown. 墨干(Mogan) literally means "dried ink". The word is derived from 莫干(Mogan) which is a combination of 莫(Mo) and 干(Gan)[^2].

GNU TeXmacs is for Scientists, Mogan is for Writers.

## Plans
Mogan v1.0 (based on GNU TeXmacs 2.1.1) will be a baseline release with the following three major changes:
+ Switch from GNU Guile 1.8 to S7
+ Switch from Qt 4.x to Qt 5.x
+ Change some of the default settings

Mogan v1.x will always be compatible with GNU TeXmacs 2.1.1.

## How to Get Involved
+ Join us on [Discussions](https://github.com/XmacsLabs/mogan/discussions)
+ Report bugs or wishes to [Issues](https://github.com/XmacsLabs/mogan/issues)
+ Show me your code at [Pull Requests](https://github.com/XmacsLabs/mogan/pulls)

[^1]: https://en.wikipedia.org/wiki/Mount_Mogan
[^2]: https://en.wikipedia.org/wiki/Gan_Jiang_and_Mo_Ye
