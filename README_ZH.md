# 墨干编辑器 / [Mogan Editor](README.md)
墨干编辑器是一个基于GNU TeXmacs的结构化编辑器。

墨干得名于家乡的避暑胜地——莫干山。犹忆青春年少时，在高中同桌老家，靠近莫干山的一个小山村里面，住过一晚上。傍晚，我们就在屋后小溪里面洗澡。清晨，跟着他在山上兜兜转转挖笋。中午，一起吃刚挖好的笋。溪水潺潺，篁竹幽幽，炊烟袅袅……

## 计划
墨干V1.0将会是一个基础版本，基于GNU TeXmacs 2.1.1，主要有三大重要变更：
+ 从GNU Guile 1.8切换到S7
+ 从Qt 4切换到Qt 5
+ 更改一些默认配置项


墨干V1.x将始终和GNU TeXmacs 2.1.1保持兼容。

## 如何参与
+ [加入](https://github.com/XmacsLabs/mogan/discussions)讨论
+ [报告](https://gitee.com/XmacsLabs/mogan/issues)错误或者改进
+ [贡献](https://github.com/XmacsLabs/mogan/pulls)你的代码
