<TeXmacs|2.1>

<style|<tuple|tmdoc|chinese|old-spacing|old-dots|old-lengths>>

<\body>
  <tmdoc-title|Welcome to <name|Mogan Editor>>

  Please read the Chinese welcome document:
  <hlink|\<#6B22\>\<#8FCE\>\<#4F7F\>\<#7528\>\<#58A8\>\<#5E72\>\<#7F16\>\<#8F91\>\<#5668\>|welcome.zh.tm>.

  Detailed release notes are available on
  <hlink|Github|https://github.com/XmacsLabs/mogan/releases>.

  <tmdoc-copyright|2020\U2021|<name|Xmacs> Labs>
</body>

<initial|<\collection>
</collection>>