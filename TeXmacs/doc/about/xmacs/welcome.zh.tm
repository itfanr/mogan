<TeXmacs|2.1>

<style|<tuple|tmdoc|chinese|old-spacing|old-dots|old-lengths>>

<\body>
  <\hide-preamble>
    <assign|xmacs-version|v1.0-alpha4>
  </hide-preamble>

  <tmdoc-title|\<#6B22\>\<#8FCE\>\<#4F7F\>\<#7528\>\<#58A8\>\<#5E72\>\<#7F16\>\<#8F91\>\<#5668\><value|xmacs-version>>

  \<#58A8\>\<#5E72\>\<#7F16\>\<#8F91\>\<#5668\>\<#884D\>\<#751F\>\<#81EA\>GNU
  <TeXmacs>\<#3002\>\<#4E0E\><TeXmacs>\<#6709\>\<#4EE5\>\<#4E0B\>\<#533A\>\<#522B\>\<#FF1A\>

  <\itemize>
    <item>\<#6E90\>\<#4EE3\>\<#7801\>\<#FF1A\>\<#58A8\>\<#5E72\>\<#4F1A\>\<#57FA\>\<#4E8E\>\<#67D0\>\<#4E00\>\<#4E2A\>\<#7248\>\<#672C\>\<#7684\><TeXmacs>\<#5B9A\>\<#5236\>\<#FF0C\>\<#9075\>\<#5FAA\>GPL
    v3\<#5F00\>\<#6E90\>\<#534F\>\<#8BAE\>\<#FF0C\>\<#6BD4\>\<#5982\>\<#60A8\>\<#73B0\>\<#5728\>\<#6B63\>\<#5728\>\<#4F7F\>\<#7528\>\<#7684\>\<#58A8\>\<#5E72\>\<#7F16\>\<#8F91\>\<#5668\><value|xmacs-version>\<#57FA\>\<#4E8E\><TeXmacs>
    <TeXmacs-version>

    <item>\<#754C\>\<#9762\>\<#FF1A\>\<#4ECE\>Qt
    4.x\<#5347\>\<#7EA7\>\<#5230\>\<#4E86\>Qt 5.x

    <item>\<#811A\>\<#672C\>\<#5F15\>\<#64CE\>\<#FF1A\>\<#4ECE\>Guile
    1.8\<#5207\>\<#6362\>\<#5230\>\<#4E86\>S7
  </itemize>

  <section|\<#7279\>\<#6027\>\<#4E00\>\<#89C8\>>

  <section|\<#8054\>\<#7CFB\>\<#6211\>\<#4EEC\>>

  <\big-table|<tabular|<tformat|<cwith|1|1|1|-1|cell-background|#ffa>|<cwith|2|-1|1|-1|cell-halign|c>|<table|<row|<cell|\<#6E20\>\<#9053\>>|<cell|\<#5907\>\<#6CE8\>>>|<row|<cell|\<#90AE\>\<#7BB1\>>|<cell|sadhen@zoho.com.cn>>|<row|<cell|\<#5FAE\>\<#4FE1\>\<#516C\>\<#4F17\>\<#53F7\>>|<cell|Xmacs>>|<row|<cell|\<#5FAE\>\<#4FE1\>\<#53F7\>>|<cell|XmacsLabs>>|<row|<cell|GNU
  <TeXmacs>\<#4E2D\>\<#6587\>\<#793E\>\<#533A\>QQ\<#7FA4\>>|<cell|934456971>>>>>>
    \<#8054\>\<#7CFB\>\<#65B9\>\<#5F0F\>
  </big-table>

  <tmdoc-copyright|2020-2021<strong|>|<name|Xmacs> Labs>
</body>

<initial|<\collection>
</collection>>
